﻿
var tags = null;
$("#tags").keyup(function () {
    
    clearTimeout(tags);
    tags = setTimeout(function () {
        if ($("#tags").val().length < 3) {
            return false;
        }
        $('#loading').html('<img src="../loading.gif">');

        var URL = window.location + "/Home/Share?value=" + $("#tags").val();
        $("#link").attr("href", URL);

        $.post(window.location + "/Home/GetImages",
            { "tags": $("#tags").val() },
            function (data) {

            var container = $("#container");
            container.html("");

            $.each(data, function () {

                var thumb = $("<div>").addClass("picture");

                var image = $("<img>")
                    .attr({
                        "src": this.ImageUrl,
                        "alt": "Picture not available"
                    }).on('load', function () { $(this).fadeIn("slow"); });

                var title = $("<span>")
                    .html(this.Title);

                thumb.append(title);
                thumb.append(image);

                container.append(thumb);
            });
            $('#loading').html("");
        }, "json");
    }, 250);

})

