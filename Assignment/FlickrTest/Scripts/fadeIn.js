﻿$('body').ready(function () {
    $('img').each(function (i) {
        if (this.complete) {
            $(this).fadeIn();
        } else {
            $(this).load(function () {
                $(this).fadeIn();
            });
        }
    });
});