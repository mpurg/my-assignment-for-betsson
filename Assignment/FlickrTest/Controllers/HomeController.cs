﻿using System.Web.Mvc;
using FlickrTest.Repository;
using System.Collections.Generic;

namespace FlickrTest.Controllers
{
    /// <summary>
    /// The home controller
    /// </summary>
    public class HomeController : Controller
    {
        List<IRepository> repositories;
        public HomeController()
        {
            repositories = new List<IRepository>();
            repositories.Add(new FlickrRepository());
            repositories.Add(new FlickrCacheableRepository());
        }
        /// <summary>
        /// GET: /Home/
        /// </summary>
        /// <returns>Returns an action result containing the view</returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Gets images from repository
        /// </summary>
        /// <param name="tags">Tags that should be searched for in the repository</param>
        /// <returns>A Json object containing the images from the repository</returns>
        [HttpPost]
        public ActionResult GetImages(string tags) {
            var repo = repositories[1];
            return Json(repo.GetImagesByTags(tags));
        }

        public ActionResult Share(string value)
        {
            return View(repositories[1].GetImagesByTags(value));
        }

    }
}
